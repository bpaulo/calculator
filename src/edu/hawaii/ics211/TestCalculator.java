package edu.hawaii.ics211;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;

/**
 * TestCalculator, test class for calculator.
 * 
 * @author Paulo Baldovi
 *
 */
public class TestCalculator {

  /** The calculator for testing. */
  private Calculator calc;


  /** Sets up the calculator. */
  @Before
  public void setUp() {
    this.calc = new Calculator();
  }


  /** Tests the add(int, int) method. */
  @Test
  public void testAdd() {
    assertEquals("The answer should be 2", 2, this.calc.add(1, 1));
    assertEquals("The answer should be 0", 0, this.calc.add(1, -1));

    // add method returns -2147483648
    assertFalse("The answer should be 2147483648",
        Integer.MAX_VALUE < this.calc.add(Integer.MAX_VALUE, 1));
  }


  /** Tests the subtract(int, int) method. */
  @Test
  public void testSubtract() {
    assertEquals("The answer should be 0", 0, this.calc.subtract(1, 1));
    assertEquals("The answer should be -4", -4, this.calc.subtract(1, 5));

    // substract method returns 2147483647
    assertFalse("The answer should be -2147483649",
        Integer.MIN_VALUE > this.calc.subtract(Integer.MIN_VALUE, 1));
  }


  /** Tests the multiply(int, int) method. */
  @Test
  public void testMultiply() {
    assertEquals("The answer should be 18", 18, this.calc.multiply(9, 2));
    assertEquals("The answer should be -18", -18, this.calc.multiply(9, -2));

    // multiply method returns -2
    assertFalse("The answer should be >2147483647",
        Integer.MAX_VALUE < this.calc.multiply(Integer.MAX_VALUE, 2));
  }


  /** Tests the divide(int, int) method. */
  @Test
  public void testDivide() {
    assertEquals("The answer should be 5", 5, this.calc.divide(10, 2));
  }


  /** Tests the divide(int, int) method to throw exception. */
  @Test(expected = ArithmeticException.class)
  public void testDivideException() throws ArithmeticException {
    this.calc.divide(1, 0);
  }


  /** Tests the modulo(int, int) method. */
  @Test
  public void testModulo() {
    assertEquals("The answer should be 2", 2, this.calc.modulo(12, 10));
  }


  /** Tests the modulo(int, int) method to throw exception. */
  @Test(expected = ArithmeticException.class)
  public void testModuloException() throws ArithmeticException {
    this.calc.modulo(1, 0);
  }


  /** Tests the pow(int, int) method. */
  @Test
  public void testPow() {
    assertEquals("The answer should be 100", 100, this.calc.pow(10, 2));

    // pow method returns 1
    assertFalse("The answer should be -1", -1 == this.calc.pow(-1, -1));
  }

}
